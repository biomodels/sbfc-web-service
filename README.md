                        S B F C Webservice Client

The **System Biology Format Converter** (SBFC) aims is to provide a generic framework that potentially allows any conversion between two formats. Interoperability between formats is a recurring issue in Systems Biology. Although there are various tools available to convert models from one format to another, most of them have been independently developed and cannot easily be combined, specially to provide support for more formats. 

This project contain few classes to help you to use the **SBFC webservices**. The main class is **SBFCWebServiceClient**, the class UsageExample show you examples on how to use the SBFCWebServiceClient methods.


For more information about the SBFC Framework, please visit the SBFC website at:

<http://sbfc.sourceforge.net/>

Please report problems with SBFC using the tracker at  <http://sourceforge.net/p/sbfc/bugs/>

Mailing list for discussing SBFC is at <https://groups.google.com/d/forum/sbfc-forum> (sbfc-forum at googlegroups.com). Please use this mailing list or 'sbfc-devel at googlegroups.com' to contact the core SBFC developers directly.

---


Thank you for your interest in SBFC!

The SBFC Team.

                                                                                                

<pre>
_____/\\\\\\\\\\\______/\\\\\\\\\\\\\_______/\\\\\\\\\\\\\\\____/\\\\\\\\\_        
 ___/\\\/////////\\\___\/\\\/////////\\\____\/\\\///////////___/\\\////////__       
  __\//\\\______\///____\/\\\_______\/\\\____\/\\\____________/\\\/___________      
   ___\////\\\___________\/\\\\\\\\\\\\\\_____\/\\\\\\\\\\\___/\\\_____________     
    ______\////\\\________\/\\\/////////\\\____\/\\\///////___\/\\\_____________    
     _________\////\\\_____\/\\\_______\/\\\____\/\\\__________\//\\\____________   
      __/\\\______\//\\\____\/\\\_______\/\\\____\/\\\__________\///\\\__________  
       _\///\\\\\\\\\\\/_____\/\\\\\\\\\\\\\/_____\/\\\____________\////\\\\\\\\\_ 
        ___\///////////_______\/////////////_______\///_______________\/////////__
</pre>
