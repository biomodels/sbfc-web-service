/*
 *
 * ==========================================================================
 * This file is part of The System Biology Format Converter (SBFC).
 * Please visit <http://sbfc.sf.net> to have more information about
 * SBFC. 
 * 
 * Copyright (c) 2010-2016 jointly by the following organizations:
 * 1. EMBL European Bioinformatics Institute (EBML-EBI), Hinxton, UK
 * 2. The Babraham Institute, Cambridge, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution
 * and also available online as 
 * <http://sbfc.sf.net/mediawiki/index.php/License>.
 * 
 * ==========================================================================
 * 
 */

package org.sbfc.ws;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Example class for using the SBFC web Service
 * 
 * @author rodrigue
 *
 */
public class UsageExample {

  /**
   * @param args the arguments of the program
   * @throws IOException if there is a problem reading one of the input files
   * @throws InterruptedException if any thread has interrupted the current thread. The interrupted status of the current thread is cleared when this exception is thrown.
   */
  public static void main(String[] args) throws IOException, InterruptedException {


    if(args.length == 0 && args[0].endsWith(".xml")) {
      System.out.println("To run this example, an SBML model must be passed as input parameter.");
      return;
    }


    // Converted model Result
    String convertionResult = "";

    // Creating the link to the web service
    SBFCWebServiceClient link = new SBFCWebServiceClient();

    // You can also initialize the WS link using your own url to connect to a private server
    // SBFCWebServiceClient link = new SBFCWebServiceClient("http://localhost:8080/biomodels/tools/converters/webService");




    // first conversion using the method submitAndGetResultFromURL
    System.out.println("Launching the first conversion job...\n\n");
    // URL pointing to the model to convert
    String modelURL = "http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3668293/bin/1752-0509-7-41-S11.xml";

    // input Model format
    String inputType = ConverterParam.SBMLModel;

    // Converter to use
    String converter = ConverterParam.SBML2XPP;


    // Launching the first conversion: SBML url to XPP
    convertionResult = link.submitAndGetResultFromURL(modelURL, inputType, converter);

    // Printing the result
    System.out.println(convertionResult);

    PrintWriter out = new PrintWriter(new FileWriter("sbfc-ws-test.xpp"));
    out.print(convertionResult);
    out.close();





    // second conversion using the method submitAndGetResultFromFile

    String sbmlModelPath = args[0];
    converter = ConverterParam.SBML2SBML_L3_V1;

    System.out.print("Launching the second conversion job...");

    // Launching the second conversion: SBML file to SBML level 3 version 1
    String sbmlL3String = link.submitAndGetResultFromFile(sbmlModelPath, inputType, converter);
    System.out.print("  DONE\n\n");

    System.out.println(sbmlL3String);

    // third conversion using the method submitAndGetResultFromString

    converter = ConverterParam.SBML2Matlab;

    System.out.println("Launching the third conversion job: \n\n");






    // Launching the third conversion: SBML String to Matlab
    convertionResult = link.submitAndGetResultFromString(sbmlL3String, inputType, converter);

    // Printing the result
    System.out.println(convertionResult);

    out = new PrintWriter(new FileWriter("sbfc-ws-test.m"));
    out.print(convertionResult);
    out.close();






    // fourth conversion job using asynchronous methods
    System.out.println("\n\nLaunching the fourth conversion job...");

    converter = ConverterParam.SBML2BioPAX_l3;

    ConversionInfo metadata = link.submitJobFromFile(sbmlModelPath, inputType, converter);
    System.out.println("DONE, now waiting for the job to finished while doing other stuff");

    String status = link.getJobStatus(metadata); 

    // This while loop could be done in a separate thread to allow the rest of the application to run smoothly
    // You can also only check for the job status when the user click on a button 
    while(! status.equals(ConverterParam.Finished_Job)) {
      System.out.println("job status = " + status);
      Thread.sleep(1000); // waiting 1s between each check
      status = link.getJobStatus(metadata);
    }

    System.out.println("Now the job is finished, so we can get the results from the server\n\n");

    // Getting the job result and deleting all related files on the server side
    String dotStr = link.getConvertionResult(metadata, true);

    System.out.println(dotStr);

    out = new PrintWriter(new FileWriter("sbfc-ws-test.dot"));
    out.print(convertionResult);
    out.close();

  }
}
