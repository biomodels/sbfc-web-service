/*
 * $Id: SBFCWebServiceClient.java 583 2015-09-29 15:53:40Z niko-rodrigue $
 * $URL: svn+ssh://niko-rodrigue@svn.code.sf.net/p/sbfc/code/webServiceClient/src/org/sbfc/ws/SBFCWebServiceClient.java $
 *
 * ==========================================================================
 * This file is part of The System Biology Format Converter (SBFC).
 * Please visit <http://sbfc.sf.net> to have more information about
 * SBFC. 
 * 
 * Copyright (c) 2010-2015 jointly by the following organizations:
 * 1. EMBL European Bioinformatics Institute (EBML-EBI), Hinxton, UK
 * 2. The Babraham Institute, Cambridge, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution
 * and also available online as 
 * <http://sbfc.sf.net/mediawiki/index.php/License>.
 * 
 * ==========================================================================
 * 
 */

package org.sbfc.ws;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * This class allows to request conversion jobs to the SBFC web-service
 * 
 * @author jpettit
 * @author rodrigue
 * 
 */
public class SBFCWebServiceClient {
  
  
	/**
	 * Location of the webService
	 */
	private String SBFC_WEB_SERVICE_URL = "https://www.ebi.ac.uk/biomodels/tools/converters/webService";

	/**
	 * Interval of time in millisecond between two check for a job status 
	 */
	public int JOB_STATUS_INTERVAL = 3000;
	
	/**
	 * Creates a new instance of {@link SBFCWebServiceClient}
	 */
	public SBFCWebServiceClient() {}
	
    /**
     * Creates a new instance of {@link SBFCWebServiceClient} connected
     * to the given url.
     * 
     * @param sbfcWebServiceURL the url of an SBFC webService
     */
    public SBFCWebServiceClient(String sbfcWebServiceURL) {
      this.SBFC_WEB_SERVICE_URL = sbfcWebServiceURL;
    }
	

    /**
     * Submits a conversion Job from a file and returns some metadata about the job.
     * 
     * @param filePath the path of the file to read
     * @param inputModelType the class name of the input format
     * @param converterType the class name of the converter
     * @return some metadata about the conversion job.
     */
    public ConversionInfo submitJobFromFile(String filePath, String inputModelType, String converterType) {
        ConversionInfo convertionId = new ConversionInfo(); 
        
        String model = "";
        try {
          model = readFileAsString(filePath);
        } catch (IOException e) {
          throw new RuntimeException(e);
        }
        
        //Build parameter string
        String data = "method=submit&inputModelType="+inputModelType+"&converterType="+converterType+"&text_upload="+model;

        
        //Sending POST request to the server
        sendPostRequest(data, convertionId);
        
        return convertionId;
    }
    

	/**
	 * Submits a conversion Job from a String containing the input format and returns some metadata about the job.
	 * 
	 * @param model a String containing the input format
	 * @param inputModelType the class name of the input format
	 * @param converterType the class name of the converter
	 * @return some metadata about the conversion job.
	 */
	public ConversionInfo submitJobFromString(String model,String inputModelType, String converterType) {
		ConversionInfo convertionId = new ConversionInfo();	
		
        //Build parameter string
        String data = "method=submit&inputModelType="+inputModelType+"&converterType="+converterType+"&text_upload="+model;

        
        //Sending POST request to the server
        sendPostRequest(data, convertionId);
        
        return convertionId;
	}
	
	
	/**
	 * Submits a conversion Job from a URL pointing to the input format and returns some metadata about the job.
	 * 
	 * @param url a URL pointing to the input format
	 * @param inputModelType the class name of the input format
	 * @param converterType the class name of the converter
	 * @return some metadata about the conversion job.
	 */
	public ConversionInfo submitJobFromURL(String url,String inputModelType, String converterType) {
		ConversionInfo convertionId = new ConversionInfo();	
		
        //Build parameter string
        String data = "method=submit&inputModelType="+inputModelType+"&converterType="+converterType+"&url_upload="+url;

        
        //Sending POST request to the server
        sendPostRequest(data, convertionId);
        
        return convertionId;
	}
	
	
	/**
	 * Returns the status of a conversion job on the webservice
	 * 
	 * @param convertionId some metadata about the conversion job.
	 * @return "pending" | "done" | "unknown" | "not found"
	 */
	public String getJobStatus(ConversionInfo convertionId) {
		String status=null;
		
		//Build parameter string
		String data = "method=getStatus&jobIdent="+convertionId.getIdentificationId() 
            + "&sessionId=" + convertionId.getSessionId() + "&jobId=" + convertionId.getJobId();
	
		//sending POST request to the server
		status = sendPostRequest(data, new ConversionInfo());
		
		return status;
		
		
	}
	
    /**
     * Submits the conversion job from the given file path then waits for the end of the conversion and returns the result
     * 
     * @param filePath the path of the input file
     * @param inputModelType the class name of the input format
     * @param converterType the class name of the converter
     * @return the converted format as a String or the error log if a problem occurred during the conversion
     * @throws InterruptedException if any thread has interrupted the current thread. The interrupted status of the current thread is cleared when this exception is thrown.
     */
    public String submitAndGetResultFromFile(String filePath,String inputModelType, String converterType) throws InterruptedException {
        ConversionInfo convert = submitJobFromFile(filePath, inputModelType, converterType);
        
        String status = getJobStatus(convert); 
        while(! status.equals(ConverterParam.Finished_Job)) {
          Thread.sleep(JOB_STATUS_INTERVAL);
          status = getJobStatus(convert);
        }
        
        return getConvertionResult(convert, false);
    }
	
	/**
	 * Submits the conversion job from the model String then waits for the end of the conversion and returns the result
	 * 
	 * @param model a String containing the input format
	 * @param inputModelType inputModel format, We strongly recommend you use one of ConverterParam attributes (ex: ConvertParam.SBMLModel)
	 * @param converterType inputModel to use, We strongly recommend you use one of ConverterParam attributes (ex: ConvertParam.SBML_2_BioPAX_l2v3)
	 * @return the converted format as a String or the error log if a problem occurred during the conversion
	 * @throws InterruptedException if any thread has interrupted the current thread. The interrupted status of the current thread is cleared when this exception is thrown.
	 */
	public String submitAndGetResultFromString(String model,String inputModelType, String converterType) throws InterruptedException {
		ConversionInfo convert = submitJobFromString(model, inputModelType, converterType);
		
        String status = getJobStatus(convert); 
        while(! status.equals(ConverterParam.Finished_Job)) {
          Thread.sleep(JOB_STATUS_INTERVAL);
          status = getJobStatus(convert);
        }
		
		return getConvertionResult(convert, false);
	}
	
	
	/**
	 * Submits the conversion job from the model URL then waits for the end of the conversion and returns the result
	 * 
	 * @param url a URL pointing to the input format
	 * @param inputModelType the class name of the input format
	 * @param converterType the class name of the converter
	 * @return the converted format as a String or the error log if a problem occurred during the conversion
	 * @throws InterruptedException if any thread has interrupted the current thread. The interrupted status of the current thread is cleared when this exception is thrown.
	 */
	public String submitAndGetResultFromURL(String url,String inputModelType, String converterType) throws InterruptedException {
		ConversionInfo convert = submitJobFromURL(url, inputModelType, converterType);
		
		String status = getJobStatus(convert); 
		while(! status.equals(ConverterParam.Finished_Job)) {
		  Thread.sleep(JOB_STATUS_INTERVAL);
		  status = getJobStatus(convert);
		}

		return getConvertionResult(convert, false);
	}
	
	/**
	 * Retrieves the result format as a String.
	 * 
	 * <p>WARNING ! : if the value of the delete parameter is 'true', this method can be called only once
	 * as it will destroy all the related conversion Files on the Server"
	 * 
	 * @param convertionId some metadata about the conversion job.
	 * @param delete a boolean to tell the method to delete or not all the files related to fileName. 
	 * @return the converted format as a String or the error log if a problem occurred during the conversion
	 */
	public String getConvertionResult(ConversionInfo convertionId, boolean delete) {
		String model = null;
		
		//Build parameter string
		String data = "method=getResult&jobIdent=" + convertionId.getIdentificationId() 
		    + "&sessionId=" + convertionId.getSessionId() + "&jobId=" + convertionId.getJobId()
		     + "&delete=" + delete;
		
		//sending POST request to the server
		model = sendPostRequest(data, new ConversionInfo());
		
		return model;
	}
	
	
	/**
	 * Sends a POST request to the SBFC webservice and returns the result.
	 * 
	 * @param data the query string
	 * @return the result of the POST request to the SBFC webservice
	 */
	private String sendPostRequest(String data, ConversionInfo convertionIds) {
        String answer = "";
	    try {
	        
	        // Send the request
	        URL url = new URL(SBFC_WEB_SERVICE_URL);
	        
	        URLConnection conn = url.openConnection();
	        conn.setDoOutput(true);
	        OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
	        
	        //write parameters
	        writer.write(data);
	        writer.flush();
	        
	        // Get the response

	        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	        String line;
	        while ((line = reader.readLine()) != null) {
	            answer += line;

	            if (data.startsWith("method=getResult")) {
	            	answer += "\n";
	            }
	        }
	        writer.close();
	        reader.close();
	     
	        if (answer.startsWith("JOB_SUMMARY")) {
	          String[] ids = answer.split(" ");
	          
	          convertionIds.setIdentificationId(ids[1] + "_" + ids[2]);
	          
	          convertionIds.setSessionId(ids[2]);
	          convertionIds.setJobId(ids[3]);
	          
	          // System.out.println("Found the following ids for this job: " + convertionIds);
	        }
	        
	    } catch (MalformedURLException ex) {
	        ex.printStackTrace();
	    } catch (IOException ex) {
	        ex.printStackTrace();
	    }
	    return answer;
	}

	/**
	 * Returns the content of a file as a String.
	 * 
	 * @param filePath the path of the File to read
	 * @return the content of a file as a String.
	 * @throws IOException - If an I/O error occurs
	 */
	public static String readFileAsString(String filePath) throws IOException{
	  BufferedReader in = new BufferedReader(new FileReader(filePath));
	  String result="";
	  String line;
	  
	  while ((line = in.readLine()) != null) {
	    result+= line + System.getProperty("line.separator");
	  }
	  in.close();
	  return result;
	}

}
