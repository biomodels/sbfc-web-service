/*
 * $Id: ConverterParam.java 583 2015-09-29 15:53:40Z niko-rodrigue $
 * $URL: svn+ssh://niko-rodrigue@svn.code.sf.net/p/sbfc/code/webServiceClient/src/org/sbfc/ws/ConverterParam.java $
 *
 * ==========================================================================
 * This file is part of The System Biology Format Converter (SBFC).
 * Please visit <http://sbfc.sf.net> to have more information about
 * SBFC. 
 * 
 * Copyright (c) 2010-2015 jointly by the following organizations:
 * 1. EMBL European Bioinformatics Institute (EBML-EBI), Hinxton, UK
 * 2. The Babraham Institute, Cambridge, UK
 *
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation. A copy of the license agreement is provided
 * in the file named "LICENSE.txt" included with this software distribution
 * and also available online as 
 * <http://sbfc.sf.net/mediawiki/index.php/License>.
 * 
 * ==========================================================================
 * 
 */
package org.sbfc.ws;

/**
 * This class is just an helper for launching conversion job,
 *  it registers available converters, input formats and web service response
 * @author jpettit
 *
 */
public class ConverterParam {
	
		
	/*Available Model format for input */
	static String SBMLModel = "SBMLModel";
	static String BioPAXModel = "BioPAXModel";
	static String XPPModel = "XPPModel";	
	static String APMModel = "APMModel";
	static String DotModel = "DotModel";
	static String GPMLModel = "GPMLModel";
	static String OctaveModel = "OctaveModel";
	static String SBGNModel = "SBGNModel";

	
	
	/*Available converters */
	static String SBML2BioPAX_l2 = "SBML2BioPAX_l2";
	static String SBML2BioPAX_l3 = "SBML2BioPAX_l3";
	static String SBML2XPP = "SBML2XPP";	
	static String BioPAXL3Converter = "BioPAXL3Converter";
	static String SBML2APM = "SBML2APM";
	static String SBML2Dot = "SBML2Dot";
	static String SBML2Matlab = "SBML2Matlab";
	static String SBML2Octave = "SBML2Octave";
	static String SBML2SBGNML = "SBML2SBGNML";
	static String SBML2SBML_L3_V1 = "SBML2SBML_L3V1";
	static String SBML2SBML_L2_V4 = "SBML2SBML_L2V4";
	static String URL2URN = "URL2URN";
	static String URN2URL = "URN2URL";

	
	
	/*Convertion jobs status*/
	static String Pending_Job = "pending";
	static String Finished_Job = "done";
	static String Not_Found_Job = "not found";
	static String Unknown_Job_Status = "unknown";

}
